# BTP_Roaming_Expt

Compile Instructions:

1) g++ -o udp_server udp_server.cpp -lpthread
2) g++ -std=c++11 -o udp_client udp_client.cpp

To run:
1) ./udp_client ---- do in Onboard controller (Client)
2) ./udp_server ---- do in Global controller (Server)

====== Timed_connection.sh =======
----- Run timed_connection.sh in PAPs (Access Points)
----- In timed_connection.sh replace MAC_ADDR of LAT with your Station's MAC address

====== send_signal.sh =====
----- Run send_signal.sh in Global controller (Server)