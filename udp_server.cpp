#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <vector>
#include <string>
#define SERVER_PORT 53013
#define BUF_SIZE 256
#define NO_OF_PACKETS 1000*10
#define LAST_K_PACKETS 500
#define SLEEP_TIME_SIGNAL (20*1000)

using namespace std;

int roaming = 0;

struct recv_data {
	vector<int> packets;
};

struct recv_data rd;

void error(const char *msg){
	perror(msg);
	exit(EXIT_FAILURE);
}

void *recv_routine(void *threadarg){
	struct recv_data *my_data = (struct recv_data *)threadarg;

	int sockfd;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	struct sockaddr_in serv,client;

	serv.sin_family = AF_INET;
	serv.sin_port = htons(SERVER_PORT);
	serv.sin_addr.s_addr = inet_addr("10.0.0.1");

	if (bind(sockfd, (struct sockaddr *)&serv, sizeof(serv)) < 0) {
		error("bind failed");
	}

	char buffer[BUF_SIZE];
	socklen_t l = sizeof(client);

	int ii = 0, seqnum = 0, prev_seq = 0;
	
	while(ii <NO_OF_PACKETS){
		int rc= recvfrom(sockfd,buffer,sizeof(buffer),0,(struct sockaddr *)&client,&l);
		memcpy(&seqnum, buffer, 4);
		if(roaming == 1){
			cout << "Jump in Sequence No: " << (seqnum-prev_seq) << ",    Roaming time: " << ((seqnum-prev_seq)*1) << "ms"<< endl;
			roaming = 0;
		}
		if(rc<0){
			cout<<"ERROR READING FROM SOCKET";
		}
		// my_data->packets.push_back(seqnum);
		prev_seq = seqnum;
		ii++;
	}
	close(sockfd);
	
	pthread_exit(NULL);
}

void SIGUSR1_handler(int sig){
	roaming = 1;
	// system("date +%s%3N");
}


int main(){
	pthread_t recv_thread;
	int rc;
	void *status;
	pid_t pid = getpid();

	cout << "pid: " << pid << endl;

	// register signal handler
	if(signal(SIGUSR1, SIGUSR1_handler) == SIG_ERR){
		printf("SIGUSR1 install error\n");
		return 0;
	}

	cout <<"main() : creating recv_thread" << endl;
	rc = pthread_create(&recv_thread, NULL, recv_routine, (void *)(&rd));
	if(rc){
		cout << "Error:unable to create thread," << rc << endl;
	}

	rc = pthread_join(recv_thread, &status);
	if(rc){
		cout << "Error:unable to join," << rc << endl;
	}

	return 0;
}
