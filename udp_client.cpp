#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#define SLEEP_TIME (1000*1)
#define SERVER_PORT 53013
#define BUF_SIZE 256
#define NO_OF_PACKETS 1000*1000
#define MAX_SEQ_NO 1000000
using namespace std;

void error(char *msg){
	perror(msg);
	exit(EXIT_FAILURE);
}

int main(){
	int sockfd;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	struct sockaddr_in serv,client;

	serv.sin_family = AF_INET;
	serv.sin_port = htons(SERVER_PORT);
	serv.sin_addr.s_addr = inet_addr("10.0.0.1");

	socklen_t l = sizeof(client);
	socklen_t m = sizeof(serv);
	
	int seq = 0, ii = 0;
	struct timeval tp;
	long int ms;
	char buffer[BUF_SIZE];

	while(ii < NO_OF_PACKETS){
		// Sequence Number
		seq = (ii)%MAX_SEQ_NO;

		// construsting packet
		memcpy(buffer, &seq, 4);
		
		// sending packet
		sendto(sockfd,buffer,4,0,(struct sockaddr *)&serv,m);
		ii++;

		// sleep
		usleep(SLEEP_TIME);
	}

	close(sockfd);
}
