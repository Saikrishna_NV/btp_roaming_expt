#!/bin/sh

./udp_server &
serv_pid=$!;

while true
do
  nc -l 9876 | while read -r LINE; do
    kill -10 $serv_pid
  done
done
