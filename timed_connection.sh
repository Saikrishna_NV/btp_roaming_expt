#!/bin/sh

# Replace <LAT_MAC_ADDR> with your Station's MAC address

if iw dev wlan0 station dump | grep <LAT_MAC_ADDR>; then
  hostapd_cli disassociate <LAT_MAC_ADDR>;
fi

iw event | while read -r LINE; do
  case "$LINE" in *'new station <LAT_MAC_ADDR>')
    echo hello | nc 10.0.0.1 9876;
    sleep 5;
    hostapd_cli disassociate <LAT_MAC_ADDR>;
  esac
done
